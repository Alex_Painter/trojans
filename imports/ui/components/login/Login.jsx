import React from 'react'
import { Accounts } from 'meteor/accounts-base'
import { BrowserRouter as Redirect, Router, Route, Switch } from 'react-router-dom'
import Input from '../forms/Input.jsx'
import Form from '../forms/Form.jsx'
import './Login.css'
import AppHistory from '../../../router.js'

export default class Login extends React.Component {
  constructor (props) {
  	super(props)
  	this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit (e) {
  	e.preventDefault()

  	var target = $(e.target)
    var fname = target.find('#first_name').val()
    var password = target.find('#password').val()

    Meteor.loginWithPassword(fname, password, (er) => {
    	if (er) {
    		Materialize.toast(er.reason, 4000)
    	} else {
    	  AppHistory.push('/')
      	  Materialize.toast('Successfully logged in!', 4000)
    	}
    })
  }

  render () {
    const firstNameInput = <Input id='first_name' key='first' type='text' className='validate' label='First Name' onChange='' />
    const passwordInput = <Input id='password' key='pass' type='password' className='validate' label='Password' onChange='' />
    const submit = <button className='btn waves-effect waves-light col offset-s1 s2'>Submit</button>
    const inputs = [firstNameInput, passwordInput]

    const form = <Form header='Login' formClass='col offset-s4 s12' inputs={inputs} submit={submit} onSubmit={this.onSubmit} />

    return (form)
  }
}
