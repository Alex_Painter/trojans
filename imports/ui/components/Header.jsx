import React from 'react'
import { User } from '/imports/api/User.js'
import { Modal, Button } from 'react-materialize'
import LogoutModal from '/imports/ui/components/login/Logout.jsx'

import '../../stylesheets/Header.css'

export default class Header extends React.Component {
  render () {
    if (User.isLoggedIn()) {
      return (
        <nav className='nav-style'>
          <div>
            <a href='/' className='left brand-logo'>Trojans Squash</a>
            <ul id='nav-mobile' className='right hide-on-med-and-down header-items-right'>
              <li><a href='/courts'>Courts</a></li>
              <li><LogoutModal /></li>
            </ul>
          </div>
        </nav>
      )
      // if admin
    } else {
      return (
        <nav className='nav-style'>
          <div>
            <a href='/' className='left brand-logo'>Trojans Squash</a>
            <ul id='nav-mobile' className='right hide-on-med-and-down header-items-right'>
              <li><a href='/register'>Register</a></li>
              <li><a href='/login'>Log In</a></li>
            </ul>
          </div>
        </nav>
      )
    }
  }
}
