import React from 'react'
import Input from './Input.jsx'

export default class Form extends React.Component {
  render () {
    return (
      <div className='container'>
        <h3 className='form-header'>{this.props.header}</h3>
        <div className='row'>
          <form className={this.props.formClass} onSubmit={this.props.onSubmit}>
            {this.props.inputs}
            {this.props.submit}
          </form>
        </div>
      </div>
    )
  }
}
