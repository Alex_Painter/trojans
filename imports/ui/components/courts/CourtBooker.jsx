import React from 'react'
import Court from './Court.jsx'
import { Row } from 'react-materialize'

export default class CourtBooker extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
  	$('.carousel.carousel-slider').carousel({fullWidth: true})
  }

  render () {
    return (
      <Row>
        <div className='container'>
          <div className='carousel carousel-slider center z-depth-4' data-indicators='true'>
            <div className='carousel-fixed-item center' />
            <Court
              itemClass='carousel-item grey lighten-3'
              textClass='blue darken-3-text'
              header='Court 3'
            />
          </div>
        </div>
      </Row>
    )
  }
}
