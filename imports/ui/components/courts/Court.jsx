import React from 'react'

export default class Court extends React.Component {
  render () {
    return (
      <div className={this.props.itemClass} href='#one!'>
        <h2>{this.props.header}</h2>
        <p className={this.props.textClass} >This is your first panel</p>
      </div>
    )
  }
}
