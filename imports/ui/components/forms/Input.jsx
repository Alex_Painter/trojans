import React from 'react'

export default function Input (props) {
  return (
    <div className='row'>
      <div className='input-field col s4'>
        <input id={props.id} type={props.type} className={props.class}
          onClick={props.click} />
        <label htmlFor={props.for}>{props.label}</label>
      </div>
    </div>
  )
}
