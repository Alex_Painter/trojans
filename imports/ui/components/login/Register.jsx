import React from 'react'
import { Accounts } from 'meteor/accounts-base'
import { BrowserRouter as Redirect, Router, Route, Switch } from 'react-router-dom'
import Input from '../forms/Input.jsx'
import Form from '../forms/Form.jsx'
import './Login.css'
import AppHistory from '../../../router.js'

export default class Register extends React.Component {
  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit (e) {
    e.preventDefault()

    var target = $(e.target)
    var fname = target.find('#first_name').val()
    var lname = target.find('#last_name').val()
    var password = target.find('#password').val()
    var email = target.find('#email').val()
    var options = {name: fname + lname}

    var accountInfo = {
      username: fname,
      email: email,
      password: password,
      options: options
    }

    Accounts.createUser(accountInfo, (er) => {
      if (er) {
        alert(er.reason)
        console.log(er.reason)
      } else {
				// redirect to /
        AppHistory.push('/')
        Materialize.toast('Successfully registered!', 4000)
      }
    })
  };

  render () {
		// could make these into a plain dict and make Form component create the Input jsx
    const firstNameInput = <Input id='first_name' key='first' type='text' className='validate' label='First Name' onChange='' />
    const lastNameInput = <Input id='last_name' key='last' type='text' className='validate' label='Last Name' onChange='' />
    const passwordInput = <Input id='password' key='pass' type='password' className='validate' label='Password' onChange='' />
    const submit = <button className='btn waves-effect waves-light col offset-s1 s2'>Submit</button>
    const inputs = [firstNameInput, lastNameInput, passwordInput]

    const form = <Form header='Register a new user' formClass='col offset-s4 s12' inputs={inputs} submit={submit} onSubmit={this.onSubmit} />

    return (form)
		/*
						<div className="container">
				  <h3 className="form-header">Register a new account</h3>
				  <div className="row">
				    <form className="col offset-s4 s12" onSubmit={this.onSubmit}>
				      <div className="row">
				        <div className="input-field col s4">
				          <input id="first_name" type="text" className="validate"
				          		value={this.state.first_name} onChange={this.onChange}/>
				          <label htmlFor="first_name">First Name</label>
				        </div>
				        </div>
				        <div className="row">
				        <div className="input-field col s4">
				          <input id="last_name" type="text" className="validate" />
				          <label htmlFor="last_name">Last Name</label>
				        </div>
				      </div>
				      <div className="row">
				        <div className="input-field col s4">
				          <input id="password" type="password" className="validate" />
				          <label htmlFor="password">PIN</label>
				        </div>
				      </div>
				      <div className="row">
				        <div className="input-field col s4">
				          <input id="email" type="email" className="validate" />
				          <label htmlFor="email" data-error={this.state.email_er}>Email</label>
				        </div>
				        <div value={this.state.error} />
				      </div>
				      <button className="btn waves-effect waves-light col offset-s1 s2">Submit
	  				  </button>
				    </form>
				  </div>

				</div>
		*/
  }
}
