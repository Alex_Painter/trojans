import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import AppHistory from '../../router.js'

import Register from '../components/login/Register.jsx'
import Login from '../components/login/Login.jsx'
import Header from '../components/Header.jsx'
import Home from '../pages/Home.jsx'
import CourtBooker from '../components/courts/CourtBooker.jsx'

export default class MainLayout extends React.Component {
  render () {
    return (
      <Router history={AppHistory}>
        <div>
          <Header />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/register' component={Register} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/courts' component={CourtBooker} />
          </Switch>
        </div>
      </Router>
    )
  }
}
