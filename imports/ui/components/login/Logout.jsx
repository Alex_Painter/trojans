import React from 'react'
import { Modal, Button } from 'react-materialize'
import Register from '/imports/ui/components/login/Register.jsx'
import { Meteor } from 'meteor/meteor'
import AppHistory from '../../../router.js'

export default class LogoutModal extends React.Component {
  signOut () {
    console.log('signout')
  	Meteor.logout((er) => {
  	  if (er) {
  	  	Materialize.toast(er.reason, 4000)
   	    console.log(er.reason)
   	  } else {
   	  	$('#overlay_0').modal('close')
   	  	AppHistory.push('/')
   	  }
  })
  }

  render () {
    return (
      <Modal
        trigger={<a href='#'>Log Out</a>}
        modalOptions={{dismissible: false}}
        actions={
          <div>
            <Button waves='red' modal='close' flat>Cancel</Button>
            <Button waves='yellow' onClick={this.signOut} >Log Out</Button>
          </div>
        }
      >
        <p>Are you sure you want to log out?</p>
      </Modal>
    )
  }
}
